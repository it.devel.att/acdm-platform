import { task } from "hardhat/config";

async function getContract(hre: any, contractAddress: string) {
    const Token = await hre.ethers.getContractFactory("DAO");
    const contract = await Token.attach(contractAddress);
    return contract;
}

task("addProposal", "Add proposal")
    .addParam("contract", "Address of DAO contract")
    .addParam("recepient", "Address of contract for call")
    .addParam("description", "Description of proposal")
    .addParam("calldata", "Calldata")
    .setAction(async (taskArgs, hre) => {
        const dao = await getContract(hre, taskArgs.contract);
        await dao.addProposal(taskArgs.recepient, taskArgs.calldata, taskArgs.description);
        console.log("Succes added new proposal")
    });

task("vote", "Vote for proposal")
    .addParam("contract", "Address of DAO contract")
    .addParam("proposalid", "ID of proposal for vote")
    .addParam("forvote", "For vote (true or false)")
    .setAction(async (taskArgs, hre) => {
        const dao = await getContract(hre, taskArgs.contract);
        await dao.vote(taskArgs.proposalid, taskArgs.forvote);
        console.log("Succes vote for proposal")
    });

task("finish", "Vote for proposal")
    .addParam("contract", "Address of DAO contract")
    .addParam("proposalid", "ID of proposal for vote")
    .setAction(async (taskArgs, hre) => {
        const dao = await getContract(hre, taskArgs.contract);
        await dao.finish(taskArgs.proposalid);
        console.log("Succes finish proposal")
    });