import { expect } from "chai";
import { ethers, network } from "hardhat";
import { Contract, utils, BigNumber } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ACDMPlatform } from "../typechain";

describe("Stacking", function () {
  let acdmToken: Contract;
  let platform: Contract;
  let daoMock: Contract;

  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addr3: SignerWithAddress;
  let addrs: SignerWithAddress[];
  let clean: any;

  // 3 days
  const roundDuration: number = 60 * 60 * 24 * 3;

  const firstRoundTokensAmount: number = 100_000;
  const firstRoundWheiForToken: number = 10_000_000_000_000;

  const defaultTradeRefersPercent: number = 2.5;
  const defaultBuyTokensReferrer1Percent: number = 5;
  const defaultBuyTokensReferrer2Percent: number = 3;

  before(async () => {
    [owner, addr1, addr2, addr3, ...addrs] = await ethers.getSigners();

    const ACDMPlatformContract = await ethers.getContractFactory("ACDMPlatform");
    platform = await ACDMPlatformContract.deploy();

    const ACDMTokenContract = await ethers.getContractFactory("ACDMToken");
    acdmToken = await ACDMTokenContract.deploy(
      platform.address,
    );

    const DAOMockContract = await ethers.getContractFactory("DAOMock");
    daoMock = await DAOMockContract.deploy();
    await platform.updateDaoAddress(daoMock.address);

    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  afterEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [clean],
    });
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  async function increaseTime(addSeconds: number) {
    await network.provider.request({
      method: "evm_increaseTime",
      params: [addSeconds],
    });
    await network.provider.request({
      method: "evm_mine",
      params: [],
    });
  }

  async function latestBlockTimestamp(): Promise<BigNumber> {
    const latestBlock = await ethers.provider.getBlock("latest");
    return utils.parseUnits(latestBlock.timestamp.toString(), 0);
  }

  async function startPlatform() {
    await platform.startPlatform(acdmToken.address);
  }

  async function firstRoundBuyTokens(account: SignerWithAddress, amount: number) {
    const weiForToken = utils.parseUnits(firstRoundWheiForToken.toString(), 0);
    const amountBN = utils.parseUnits(amount.toString(), 0);

    await platform.connect(account).buyTokens(amount, { value: weiForToken.mul(amountBN) });
  }

  async function createOrder(account: SignerWithAddress, amount: number, weiForToken: number) {
    await acdmToken.connect(account).approve(platform.address, amount);
    await platform.connect(account).createOrder(amount, weiForToken);
  }

  it("Check revert on only dao call functions", async function () {
    await expect(platform.updateTradeRefersPercent(1)).to.be.revertedWith("OnlyDAOAction()");
    await expect(platform.updateBuyTokensReferrer1Percent(1)).to.be.revertedWith("OnlyDAOAction()");
    await expect(platform.updateBuyTokensReferrer2Percent(1)).to.be.revertedWith("OnlyDAOAction()");
  });

  it("Check success call only dao configuration functions", async function () {
    const newPercent = bigInt(400);
    let callData = platform.interface.encodeFunctionData("updateTradeRefersPercent", [newPercent]);

    await daoMock.callThroughDAO(platform.address, callData);
    expect(await platform.tradeRefersPercent()).to.eq(newPercent);

    callData = platform.interface.encodeFunctionData("updateBuyTokensReferrer1Percent", [newPercent]);

    await daoMock.callThroughDAO(platform.address, callData);
    expect(await platform.buyTokensReferrer1Percent()).to.eq(newPercent);

    callData = platform.interface.encodeFunctionData("updateBuyTokensReferrer2Percent", [newPercent]);

    await daoMock.callThroughDAO(platform.address, callData);
    expect(await platform.buyTokensReferrer2Percent()).to.eq(newPercent);
  });

  it("Success call transferBalanceToOwner through dao", async function () {
    await startPlatform();

    let callData = platform.interface.encodeFunctionData("transferBalanceToOwner", []);

    const accountBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(accountBalanceBefore.toString()).to.eq("0");

    const platformBalanceBefore = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceBefore.toString()).to.eq("100000");

    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);
    const ownerEthBalanceBefore = await ethers.provider.getBalance(owner.address);

    const weiAmount = utils.parseUnits((firstRoundWheiForToken * 10).toString(), 0)
    await platform.connect(addr2).buyTokens(10, { value: weiAmount });

    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore.add(weiAmount));

    await daoMock.connect(addr3).callThroughDAO(platform.address, callData);

    const platformEthBalanceAfterMoveToOwner = await ethers.provider.getBalance(platform.address);
    const ownerEthBalanceAfter = await ethers.provider.getBalance(owner.address);


    expect(platformEthBalanceAfterMoveToOwner).to.eq(bigInt(0));
    expect(ownerEthBalanceAfter).to.eq(ownerEthBalanceBefore.add(platformEthBalanceAfter));
  });

  it("Check revert on only dao functions", async function () {
    await platform.updateDaoAddress(owner.address);
    const newPercent = bigInt(1001);
    await expect(platform.updateTradeRefersPercent(newPercent)).to.be.revertedWith("PercentExceedAmount(1000)");
    await expect(platform.updateBuyTokensReferrer1Percent(newPercent)).to.be.revertedWith("PercentExceedAmount(1000)");
    await expect(platform.updateBuyTokensReferrer2Percent(newPercent)).to.be.revertedWith("PercentExceedAmount(1000)");
  });

  it("Check price formula calculation", async function () {
    const lastPrice = 10_000_000_000_000

    const nextPriceForToken = await platform.priceForToken(lastPrice);
    const expecterNextPrice = 14_300_000_000_000;

    expect(nextPriceForToken.toString()).to.eq(expecterNextPrice.toString());
  });

  it("Owner start platform", async function () {
    const expectedWeiforToken = 10_000_000_000_000;
    await platform.startPlatform(acdmToken.address);
    const lastBlockTS = await latestBlockTimestamp()
    const saleStat = await platform.saleStats(1);

    expect(saleStat.startTime).to.eq(lastBlockTS);
    expect(saleStat.initialTokenAmount.toString()).to.eq("100000");
    expect(saleStat.leftTokens).to.eq("100000");
    expect(saleStat.weiForToken).to.eq(expectedWeiforToken.toString());
  });

  it("Only owner can start platform", async function () {
    await expect(platform.connect(addr1).startPlatform(acdmToken.address)).to.be.revertedWith("OnlyOwnerAction()");
  });

  it("Can't start platfrom twice", async function () {
    await platform.startPlatform(acdmToken.address);
    await expect(platform.startPlatform(acdmToken.address)).to.be.revertedWith("PlatformAlreadyStarted()");
  });

  it("Can't buy tokens if platform not start", async function () {
    await expect(platform.buyTokens(100)).to.be.revertedWith("PlatformNotStartYet()");
  })

  it("Can't buy tokens if sale round time passed", async function () {
    await startPlatform();
    await increaseTime(roundDuration);

    await expect(platform.buyTokens(100)).to.be.revertedWith("RoundEnded()");
  })

  it("Can't buy tokens if not enough left", async function () {
    await startPlatform();

    await expect(platform.buyTokens(firstRoundTokensAmount + 1)).to.be.revertedWith("NotEnoughTokensLeft()");
  })

  it("Can't buy tokens if sended ether not equal required", async function () {
    await startPlatform();

    await expect(platform.buyTokens(100)).to.be.revertedWith("SendedWeiNotEqRequired(1000000000000000)");
  })

  it("Can't buy tokens if not sale round", async function () {
    await startPlatform();
    await increaseTime(roundDuration);

    await platform.finishSaleRoundAndStartTradeRound();
    const tx = platform.buyTokens(10, { value: firstRoundWheiForToken * 10 });
    await expect(tx).to.be.revertedWith("NotSaleRound()");
  })

  it("Success buy tokens", async function () {
    await startPlatform();

    const accountBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(accountBalanceBefore.toString()).to.eq("0");

    const platformBalanceBefore = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceBefore.toString()).to.eq("100000");

    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);

    const weiAmount = utils.parseUnits((firstRoundWheiForToken * 10).toString(), 0)
    await platform.buyTokens(10, { value: weiAmount });
    const saleStat = await platform.saleStats(1);

    expect(saleStat.initialTokenAmount.toString()).to.eq("100000") // not changed;
    expect(saleStat.weiForToken).to.eq(firstRoundWheiForToken.toString()) // not changed;
    expect(saleStat.leftTokens).to.eq("99990") // reduce;

    const accountBalanceAfter = await acdmToken.balanceOf(owner.address);
    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);
    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    expect(accountBalanceAfter.toString()).to.eq("10");
    expect(platformBalanceAfter.toString()).to.eq("99990");
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore.add(weiAmount));
  });

  function bigInt(value: number): BigNumber {
    return utils.parseUnits(value.toString(), 0)
  }

  it("Success buy tokens with one referrer", async function () {
    await startPlatform();
    const referrer1 = addr1;
    await platform.register(referrer1.address);

    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);
    const referrer1EthBalanceBefore = await ethers.provider.getBalance(referrer1.address);

    const accountBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(accountBalanceBefore.toString()).to.eq("0");

    const platformBalanceBefore = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceBefore.toString()).to.eq("100000");

    const weiAmount = utils.parseUnits((firstRoundWheiForToken * 10).toString(), 0)
    const referrer1Amount = weiAmount.div(bigInt(100)).mul(bigInt(defaultBuyTokensReferrer1Percent))
    await platform.buyTokens(10, { value: weiAmount });
    const saleStat = await platform.saleStats(1);

    expect(saleStat.initialTokenAmount.toString()).to.eq("100000") // not changed;
    expect(saleStat.weiForToken).to.eq(firstRoundWheiForToken.toString()) // not changed;
    expect(saleStat.leftTokens).to.eq("99990") // reduce;

    const accountBalanceAfter = await acdmToken.balanceOf(owner.address);
    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);
    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    const referrer1EthBalanceAfter = await ethers.provider.getBalance(referrer1.address);

    expect(accountBalanceAfter.toString()).to.eq("10");
    expect(platformBalanceAfter.toString()).to.eq("99990");
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore.add(weiAmount).sub(referrer1Amount));
    expect(referrer1EthBalanceAfter).to.eq(referrer1EthBalanceBefore.add(referrer1Amount))
  });

  it("Success buy tokens with two referrer", async function () {
    await startPlatform();
    const referrer1 = addr1;
    const referrer2 = addr2;
    await platform.register(referrer1.address);
    await platform.connect(referrer1).register(referrer2.address);

    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);
    const referrer1EthBalanceBefore = await ethers.provider.getBalance(referrer1.address);
    const referrer2EthBalanceBefore = await ethers.provider.getBalance(referrer2.address);

    const accountBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(accountBalanceBefore.toString()).to.eq("0");

    const platformBalanceBefore = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceBefore.toString()).to.eq("100000");

    const weiAmount = utils.parseUnits((firstRoundWheiForToken * 10).toString(), 0)
    const referrer1Amount = weiAmount.div(bigInt(100)).mul(bigInt(defaultBuyTokensReferrer1Percent));
    const referrer2Amount = weiAmount.div(bigInt(100)).mul(bigInt(defaultBuyTokensReferrer2Percent));

    await platform.buyTokens(10, { value: weiAmount });
    const saleStat = await platform.saleStats(1);

    expect(saleStat.initialTokenAmount.toString()).to.eq("100000") // not changed;
    expect(saleStat.weiForToken).to.eq(firstRoundWheiForToken.toString()) // not changed;
    expect(saleStat.leftTokens).to.eq("99990") // reduce;

    const accountBalanceAfter = await acdmToken.balanceOf(owner.address);
    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);
    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    const referrer1EthBalanceAfter = await ethers.provider.getBalance(referrer1.address);
    const referrer2EthBalanceAfter = await ethers.provider.getBalance(referrer2.address);

    expect(accountBalanceAfter.toString()).to.eq("10");
    expect(platformBalanceAfter.toString()).to.eq("99990");
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore.add(weiAmount).sub(referrer1Amount).sub(referrer2Amount));
    expect(referrer1EthBalanceAfter).to.eq(referrer1EthBalanceBefore.add(referrer1Amount))
    expect(referrer2EthBalanceAfter).to.eq(referrer2EthBalanceBefore.add(referrer2Amount))
  });

  it("Can't finish sale round if platform not started", async function () {
    await expect(platform.finishSaleRoundAndStartTradeRound()).to.be.revertedWith("PlatformNotStartYet()");
  })

  it("Can't finish sale round if duration not pass", async function () {
    await startPlatform();

    await expect(platform.finishSaleRoundAndStartTradeRound()).to.be.revertedWith("RoundNotEnded()");
  })

  it("Can't finish sale round if current round not sale", async function () {
    await startPlatform();
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();

    await expect(platform.finishSaleRoundAndStartTradeRound()).to.be.revertedWith("NotSaleRound()");
  })

  async function startTradeRound() {
    await startPlatform();
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
  }

  it("Success start trade round", async function () {
    await startPlatform();
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
  })

  it("Can't create order if round not trade", async function () {
    await startPlatform();
    await expect(platform.createOrder(100, 100)).to.be.revertedWith("NotTradeRound()");
  })

  it("Can't create order with zero amount", async function () {
    await startTradeRound();

    await expect(platform.connect(addr1).createOrder(0, 1000)).to.be.revertedWith("ZeroAmount()");
  })

  it("Success create order", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();

    const ordersCounterBefore = await platform.ordersCounter();
    const addr1BalanceBefore = await acdmToken.balanceOf(addr1.address);
    const platformBalanceBefore = await acdmToken.balanceOf(platform.address);

    expect(ordersCounterBefore).to.eq(utils.parseUnits("0", 0));
    expect(addr1BalanceBefore).to.eq(utils.parseUnits("1000", 0));
    expect(platformBalanceBefore).to.eq(utils.parseUnits("0", 0));

    await acdmToken.connect(addr1).approve(platform.address, 500);
    const tx = platform.connect(addr1).createOrder(500, 1000);
    await tx;

    const ordersCounterAfter = await platform.ordersCounter();
    const addr1BalanceAfter = await acdmToken.balanceOf(addr1.address);
    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);

    expect(ordersCounterAfter).to.eq(utils.parseUnits("1", 0));
    expect(addr1BalanceAfter).to.eq(utils.parseUnits("500", 0));
    expect(platformBalanceAfter).to.eq(utils.parseUnits("500", 0));

    const newOrder = await platform.tradeOrders(1);
    expect(newOrder.owner).to.eq(addr1.address);
    expect(newOrder.tokenAmount).to.eq(utils.parseUnits("500", 0));
    expect(newOrder.priceForToken).to.eq(utils.parseUnits("1000", 0));
    expect(newOrder.active).to.eq(true);

    await expect(tx).to.emit(platform, "NewOrder").withArgs(
      1,
      [
        newOrder.owner,
        newOrder.tokenAmount,
        newOrder.priceForToken,
        newOrder.active,
      ]
    );
  })

  it("Can't withdraw not owned order", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();

    await createOrder(addr1, 500, 1000);
    await expect(platform.withdrawOrder(1)).to.be.revertedWith("NotOrderOwner()");
  })

  it("Success withdraw order", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();

    await createOrder(addr1, 500, 1000);
    const newOrder = await platform.tradeOrders(1);
    expect(newOrder.owner).to.eq(addr1.address);
    expect(newOrder.tokenAmount).to.eq(utils.parseUnits("500", 0));
    expect(newOrder.priceForToken).to.eq(utils.parseUnits("1000", 0));
    expect(newOrder.active).to.eq(true);

    const addr1BalanceBefore = await acdmToken.balanceOf(addr1.address);
    const platformBalanceBefore = await acdmToken.balanceOf(platform.address);

    expect(addr1BalanceBefore).to.eq(utils.parseUnits("500", 0));
    expect(platformBalanceBefore).to.eq(utils.parseUnits("500", 0));

    const tx = platform.connect(addr1).withdrawOrder(1);
    await tx;

    const addr1BalanceAfter = await acdmToken.balanceOf(addr1.address);
    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);

    expect(addr1BalanceAfter).to.eq(utils.parseUnits("1000", 0));
    expect(platformBalanceAfter).to.eq(utils.parseUnits("0", 0));

    const orderAfter = await platform.tradeOrders(1);
    expect(orderAfter.owner).to.eq(addr1.address);
    expect(orderAfter.tokenAmount).to.eq(utils.parseUnits("0", 0));
    expect(orderAfter.priceForToken).to.eq(utils.parseUnits("1000", 0));
    expect(orderAfter.active).to.eq(false);

    await expect(tx).to.emit(platform, "OrderWithdraw").withArgs(
      1,
      [
        orderAfter.owner,
        orderAfter.tokenAmount,
        orderAfter.priceForToken,
        orderAfter.active,
      ]
    );
  })

  it("Can't buy order if round not trade", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);

    await expect(platform.buyOrder(1, 100)).to.be.revertedWith("NotTradeRound()");
  })

  it("Can't buy order if zero tokens left", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
    await createOrder(addr1, 500, 1000);

    await platform.buyOrder(1, 500, { value: 1000 * 500 });

    await expect(platform.buyOrder(500, 1000)).to.be.revertedWith("NotEnoughTokensLeft()");
  })

  it("Can't buy order if if not enough wei", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
    await createOrder(addr1, 500, 1000);

    await expect(platform.buyOrder(1, 500, { value: 999 * 500 })).to.be.revertedWith("NotEnoughWei()");
  })

  it("Success buy full order", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
    await createOrder(addr1, 500, 1000);

    const orderBefore = await platform.tradeOrders(1);
    const buyerTokenBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(orderBefore.tokenAmount).to.eq(utils.parseUnits("500", 0));
    expect(orderBefore.active).to.eq(true);
    expect(buyerTokenBalanceBefore).to.eq(utils.parseUnits("0", 0));

    const buyerWheiBalanceBefore = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceBefore = await ethers.provider.getBalance(addr1.address);
    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);

    const weiForPay = 1000 * 500;
    const percentAmountForRefers = (weiForPay / 100) * defaultTradeRefersPercent;

    const tx = await platform.buyOrder(1, 500, { value: weiForPay });
    const receipt = await tx.wait();
    const gasUsed = BigInt(receipt.cumulativeGasUsed) * BigInt(receipt.effectiveGasPrice);

    const buyerWeiBalanceAfter = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceAfter = await ethers.provider.getBalance(addr1.address);


    const expectedBuyerWeiBalance = buyerWheiBalanceBefore.sub(utils.parseUnits(weiForPay.toString(), 0)).sub(gasUsed);
    const weiForPayBigInt = utils.parseUnits(weiForPay.toString(), 0)
    const percentAmountForRefersBigInt = utils.parseUnits(percentAmountForRefers.toString(), 0);

    const referrsPercentAmount = percentAmountForRefersBigInt.add(percentAmountForRefersBigInt);
    const weiMinusPercents = weiForPayBigInt.sub(referrsPercentAmount);
    const expectedSellerBalance = sellerWheiBalanceBefore.add(weiMinusPercents);

    expect(buyerWeiBalanceAfter).to.eq(expectedBuyerWeiBalance);
    expect(sellerWheiBalanceAfter).to.eq(expectedSellerBalance);

    // If not referrers exist, platfrom get percents
    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore.add(referrsPercentAmount));

    const buyerTokenBalanceAfter = await acdmToken.balanceOf(owner.address);
    expect(buyerTokenBalanceAfter).to.eq(utils.parseUnits("500", 0));

    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceAfter).to.eq(utils.parseUnits("0", 0));

    const orderAfter = await platform.tradeOrders(1);
    expect(orderAfter.tokenAmount).to.eq(utils.parseUnits("0", 0));
    expect(orderAfter.active).to.eq(false);

    const tradeStats = await platform.tradeStats(1);
    expect(tradeStats.weiTradeAmount).to.eq(utils.parseUnits(weiForPay.toString(), 0));

    await expect(tx).to.emit(platform, "OrderBuy").withArgs(
      1,
      [
        orderAfter.owner,
        orderAfter.tokenAmount,
        orderAfter.priceForToken,
        orderAfter.active,
      ],
      500,
      weiForPay
    );
  })

  it("Success buy full order with one reffer", async function () {
    await startPlatform();
    const referrer1 = addr2;
    await platform.connect(addr1).register(referrer1.address);

    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
    await createOrder(addr1, 500, 1000);

    const orderBefore = await platform.tradeOrders(1);
    const buyerTokenBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(orderBefore.tokenAmount).to.eq(utils.parseUnits("500", 0));
    expect(orderBefore.active).to.eq(true);
    expect(buyerTokenBalanceBefore).to.eq(utils.parseUnits("0", 0));

    const buyerWheiBalanceBefore = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceBefore = await ethers.provider.getBalance(addr1.address);
    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);
    const referrer1EthBalanceBefore = await ethers.provider.getBalance(referrer1.address);

    const weiForPay = 1000 * 500;
    const percentAmountForRefers = (weiForPay / 100) * defaultTradeRefersPercent;

    const tx = await platform.buyOrder(1, 500, { value: weiForPay });
    const receipt = await tx.wait();
    const gasUsed = BigInt(receipt.cumulativeGasUsed) * BigInt(receipt.effectiveGasPrice);

    const buyerWeiBalanceAfter = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceAfter = await ethers.provider.getBalance(addr1.address);


    const expectedBuyerWeiBalance = buyerWheiBalanceBefore.sub(utils.parseUnits(weiForPay.toString(), 0)).sub(gasUsed);
    const weiForPayBigInt = utils.parseUnits(weiForPay.toString(), 0)
    const percentAmountForRefersBigInt = utils.parseUnits(percentAmountForRefers.toString(), 0);

    const referrsPercentAmount = percentAmountForRefersBigInt.add(percentAmountForRefersBigInt);
    const weiMinusPercents = weiForPayBigInt.sub(referrsPercentAmount);
    const expectedSellerBalance = sellerWheiBalanceBefore.add(weiMinusPercents);

    expect(buyerWeiBalanceAfter).to.eq(expectedBuyerWeiBalance);
    expect(sellerWheiBalanceAfter).to.eq(expectedSellerBalance);

    // Referrer 1 get 2.5 percents
    const referrer1EthBalanceAfter = await ethers.provider.getBalance(referrer1.address);
    expect(referrer1EthBalanceAfter).to.eq(referrer1EthBalanceBefore.add(percentAmountForRefersBigInt));

    // Platform get 2.5 percents
    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore.add(percentAmountForRefersBigInt));

    const buyerTokenBalanceAfter = await acdmToken.balanceOf(owner.address);
    expect(buyerTokenBalanceAfter).to.eq(utils.parseUnits("500", 0));

    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceAfter).to.eq(utils.parseUnits("0", 0));

    const orderAfter = await platform.tradeOrders(1);
    expect(orderAfter.tokenAmount).to.eq(utils.parseUnits("0", 0));
    expect(orderAfter.active).to.eq(false);

    const tradeStats = await platform.tradeStats(1);
    expect(tradeStats.weiTradeAmount).to.eq(utils.parseUnits(weiForPay.toString(), 0));

    await expect(tx).to.emit(platform, "OrderBuy").withArgs(
      1,
      [
        orderAfter.owner,
        orderAfter.tokenAmount,
        orderAfter.priceForToken,
        orderAfter.active,
      ],
      500,
      weiForPay
    );
  })

  it("Success buy full order with two refferers", async function () {
    await startPlatform();
    const referrer1 = addr2;
    const referrer2 = addr3;
    await platform.connect(addr1).register(referrer1.address);
    await platform.connect(referrer1).register(referrer2.address);

    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
    await createOrder(addr1, 500, 1000);

    const orderBefore = await platform.tradeOrders(1);
    const buyerTokenBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(orderBefore.tokenAmount).to.eq(utils.parseUnits("500", 0));
    expect(orderBefore.active).to.eq(true);
    expect(buyerTokenBalanceBefore).to.eq(utils.parseUnits("0", 0));

    const buyerWheiBalanceBefore = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceBefore = await ethers.provider.getBalance(addr1.address);
    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);
    const referrer1EthBalanceBefore = await ethers.provider.getBalance(referrer1.address);
    const referrer2EthBalanceBefore = await ethers.provider.getBalance(referrer2.address);

    const weiForPay = 1000 * 500;
    const percentAmountForRefers = (weiForPay / 100) * defaultTradeRefersPercent;

    const tx = await platform.buyOrder(1, 500, { value: weiForPay });
    const receipt = await tx.wait();
    const gasUsed = BigInt(receipt.cumulativeGasUsed) * BigInt(receipt.effectiveGasPrice);

    const buyerWeiBalanceAfter = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceAfter = await ethers.provider.getBalance(addr1.address);


    const expectedBuyerWeiBalance = buyerWheiBalanceBefore.sub(utils.parseUnits(weiForPay.toString(), 0)).sub(gasUsed);
    const weiForPayBigInt = utils.parseUnits(weiForPay.toString(), 0)
    const percentAmountForRefersBigInt = utils.parseUnits(percentAmountForRefers.toString(), 0);

    const referrsPercentAmount = percentAmountForRefersBigInt.add(percentAmountForRefersBigInt);
    const weiMinusPercents = weiForPayBigInt.sub(referrsPercentAmount);
    const expectedSellerBalance = sellerWheiBalanceBefore.add(weiMinusPercents);

    expect(buyerWeiBalanceAfter).to.eq(expectedBuyerWeiBalance);
    expect(sellerWheiBalanceAfter).to.eq(expectedSellerBalance);

    // Referrer 1 get 2.5 percents
    const referrer1EthBalanceAfter = await ethers.provider.getBalance(referrer1.address);
    expect(referrer1EthBalanceAfter).to.eq(referrer1EthBalanceBefore.add(percentAmountForRefersBigInt));

    // Referrer 2 get 2.5 percents
    const referrer2EthBalanceAfter = await ethers.provider.getBalance(referrer2.address);
    expect(referrer2EthBalanceAfter).to.eq(referrer2EthBalanceBefore.add(percentAmountForRefersBigInt));

    // If 2 referrers than platform don't get percent
    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore);

    const buyerTokenBalanceAfter = await acdmToken.balanceOf(owner.address);
    expect(buyerTokenBalanceAfter).to.eq(utils.parseUnits("500", 0));

    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceAfter).to.eq(utils.parseUnits("0", 0));

    const orderAfter = await platform.tradeOrders(1);
    expect(orderAfter.tokenAmount).to.eq(utils.parseUnits("0", 0));
    expect(orderAfter.active).to.eq(false);

    const tradeStats = await platform.tradeStats(1);
    expect(tradeStats.weiTradeAmount).to.eq(utils.parseUnits(weiForPay.toString(), 0));

    await expect(tx).to.emit(platform, "OrderBuy").withArgs(
      1,
      [
        orderAfter.owner,
        orderAfter.tokenAmount,
        orderAfter.priceForToken,
        orderAfter.active,
      ],
      500,
      weiForPay
    );
  })

  it("Success buy partial order", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();
    await createOrder(addr1, 500, 1000);

    const orderBefore = await platform.tradeOrders(1);
    const buyerTokenBalanceBefore = await acdmToken.balanceOf(owner.address);
    expect(orderBefore.tokenAmount).to.eq(utils.parseUnits("500", 0));
    expect(orderBefore.active).to.eq(true);
    expect(buyerTokenBalanceBefore).to.eq(utils.parseUnits("0", 0));

    const buyerWheiBalanceBefore = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceBefore = await ethers.provider.getBalance(addr1.address);
    const platformEthBalanceBefore = await ethers.provider.getBalance(platform.address);

    const weiForPay = 1000 * 100;
    const percentAmountForRefers = (weiForPay / 100) * defaultTradeRefersPercent;

    const tx = await platform.buyOrder(1, 100, { value: weiForPay });
    const receipt = await tx.wait();
    const gasUsed = BigInt(receipt.cumulativeGasUsed) * BigInt(receipt.effectiveGasPrice);

    const buyerWeiBalanceAfter = await ethers.provider.getBalance(owner.address);
    const sellerWheiBalanceAfter = await ethers.provider.getBalance(addr1.address);


    const expectedBuyerWeiBalance = buyerWheiBalanceBefore.sub(utils.parseUnits(weiForPay.toString(), 0)).sub(gasUsed);
    const weiForPayBigInt = utils.parseUnits(weiForPay.toString(), 0)
    const percentAmountForRefersBigInt = utils.parseUnits(percentAmountForRefers.toString(), 0);

    const referrsPercentAmount = percentAmountForRefersBigInt.add(percentAmountForRefersBigInt);
    const weiMinusPercents = weiForPayBigInt.sub(referrsPercentAmount);
    const expectedSellerBalance = sellerWheiBalanceBefore.add(weiMinusPercents);

    expect(buyerWeiBalanceAfter).to.eq(expectedBuyerWeiBalance);
    expect(sellerWheiBalanceAfter).to.eq(expectedSellerBalance);

    // If not referrers exist, platfrom get percents
    const platformEthBalanceAfter = await ethers.provider.getBalance(platform.address);
    expect(platformEthBalanceAfter).to.eq(platformEthBalanceBefore.add(referrsPercentAmount));

    const buyerTokenBalanceAfter = await acdmToken.balanceOf(owner.address);
    expect(buyerTokenBalanceAfter).to.eq(utils.parseUnits("100", 0));

    const platformBalanceAfter = await acdmToken.balanceOf(platform.address);
    expect(platformBalanceAfter).to.eq(utils.parseUnits("400", 0));

    const orderAfter = await platform.tradeOrders(1);
    expect(orderAfter.tokenAmount).to.eq(utils.parseUnits("400", 0));
    expect(orderAfter.active).to.eq(true);

    const tradeStats = await platform.tradeStats(1);
    expect(tradeStats.weiTradeAmount).to.eq(utils.parseUnits(weiForPay.toString(), 0));

    await expect(tx).to.emit(platform, "OrderBuy").withArgs(
      1,
      [
        orderAfter.owner,
        orderAfter.tokenAmount,
        orderAfter.priceForToken,
        orderAfter.active,
      ],
      100,
      weiForPay
    );
  })

  it("Can't finish trade round if round not trade", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await expect(platform.finishTradeRoundAndStartSaleRound()).to.be.revertedWith("NotTradeRound()");
  })

  it("Can't finish trade round if time not pass", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 1000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();

    await expect(platform.finishTradeRoundAndStartSaleRound()).to.be.revertedWith("RoundNotEnded()");
  })

  it("Success finish trade round and start sale round", async function () {
    await startPlatform();
    await firstRoundBuyTokens(addr1, 100_000);
    await increaseTime(roundDuration);
    await platform.finishSaleRoundAndStartTradeRound();

    const priceForToken = 10 ** 13;
    await createOrder(addr1, 50_000, priceForToken);

    const a = utils.parseUnits((50_000).toString(), 0);
    const b = utils.parseUnits(priceForToken.toString(), 0);
    const weiForPay = a.mul(b);
    await platform.buyOrder(1, 50_000, { value: weiForPay });

    await increaseTime(roundDuration);
    const tx = await platform.finishTradeRoundAndStartSaleRound();

    const latestTimestamp = await latestBlockTimestamp();

    const saleRoundCounter = await platform.saleRoundCounter();
    expect(saleRoundCounter).to.eq(utils.parseUnits("2", 0));

    const newSaleStats = await platform.saleStats(2);

    const newTokenBalance = utils.parseUnits("34965", 0);
    expect(newSaleStats.startTime).to.eq(latestTimestamp);
    expect(newSaleStats.initialTokenAmount).to.eq(newTokenBalance);
    expect(newSaleStats.leftTokens).to.eq(newTokenBalance);
    expect(newSaleStats.weiForToken).to.eq(utils.parseUnits("14300000000000", 0));

    const platformTokenBalance = await acdmToken.balanceOf(platform.address);
    expect(platformTokenBalance).to.eq(newTokenBalance);
    await expect(tx).to.emit(platform, "NewSaleRound").withArgs(
      2,
      [
        newSaleStats.startTime,
        newSaleStats.initialTokenAmount,
        newSaleStats.leftTokens,
        newSaleStats.weiForToken,
      ]
    );
  })
});
