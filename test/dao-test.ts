import { expect } from "chai";
import { ethers, network } from "hardhat";
import { Contract, utils, BigNumber } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe("DAO", function () {
  const zeroAddress: string = '0x0000000000000000000000000000000000000000';

  let voteToken: Contract;
  let dao: Contract;
  let stakingMock: Contract;

  let testContract: Contract;
  let testInterface: utils.Interface;
  let testAddress: string;
  let testCallData: string;
  let testDescription: string = "testDescription";

  let owner: SignerWithAddress;
  let chairman: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addr3: SignerWithAddress;
  let addrs: SignerWithAddress[];
  let clean: any;
  const Status = {
    Started: 0,
    Finished: 1,
    NotEnoughQuorum: 2,
    NotEnoughForVotes: 3,
    ExecutionError: 4
  }

  const initialTokenBalance: BigNumber = utils.parseUnits("100000000", 18);
  const accountsBaseBalance: BigNumber = initialTokenBalance.div(10);
  const defaultMinimumQuorum = 10;
  // 48 hours
  const defaultDebatinPeriodDurationSeconds = 60 * 60 * 24 * 2;

  before(async () => {
    [owner, chairman, addr1, addr2, addr3, ...addrs] = await ethers.getSigners();

    const TestContract = await ethers.getContractFactory("Test");
    testContract = await TestContract.deploy();
    testAddress = testContract.address;
    testInterface = testContract.interface;
    testCallData = testInterface.encodeFunctionData("setVariable1", [bigInt("999")])

    const Token = await ethers.getContractFactory("VoteToken");
    voteToken = await Token.deploy(
      "VoteToken",
      "VTN",
      initialTokenBalance
    );
    await voteToken.transfer(addr1.address, accountsBaseBalance);
    await voteToken.transfer(addr2.address, accountsBaseBalance);
    await voteToken.transfer(addr3.address, accountsBaseBalance);

    const DAOContract = await ethers.getContractFactory("DAO");
    dao = await DAOContract.deploy(
      chairman.address,
      voteToken.address,
      defaultMinimumQuorum,
      defaultDebatinPeriodDurationSeconds,
    );
    await testContract.setDao(dao.address);

    const StakingMockContract = await ethers.getContractFactory("StakingMock");
    stakingMock = await StakingMockContract.deploy();
    
    await dao.connect(chairman).setStakingContract(stakingMock.address);

    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  afterEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [clean],
    });
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: [],
    });
  });

  function genTestContractCallData(functionName: string, args: readonly any[] | undefined): string {
    return testInterface.encodeFunctionData(functionName, args)
  }

  async function increaseTime(addSeconds: number) {
    await network.provider.request({
      method: "evm_increaseTime",
      params: [addSeconds],
    });
    await network.provider.request({
      method: "evm_mine",
      params: [],
    });
  }

  function bigInt(value: string): BigNumber {
    return utils.parseUnits(value, 0);
  }

  async function latestBlockTimestamp(): Promise<BigNumber> {
    const latestBlock = await ethers.provider.getBlock("latest");
    return utils.parseUnits(latestBlock.timestamp.toString(), 0);
  }

  async function addProposal(address: string, callData: string, description: string) {
    return await dao.connect(chairman).addProposal(address, callData, description);
  }

  async function deposit(account: SignerWithAddress, amount: BigNumber) {
    await stakingMock.updateDeposit(account.address, amount);
  }

  async function vote(proposalID: number, account: SignerWithAddress, forVote: boolean, amount: BigNumber) {
    await deposit(account, amount);
    await dao.connect(account).vote(proposalID, forVote)
  }

  it("Success get config", async function () {
    const [debatingPeriod, minimumQuorum] = await dao.getConfig();

    expect(debatingPeriod).to.eq(defaultDebatinPeriodDurationSeconds);
    expect(minimumQuorum).to.eq(defaultMinimumQuorum);
  });

  it("Success update debating preriod", async function () {
    const newDebatingPeriodDuration = 1000;
    await dao.connect(chairman).updateMinDebatingPeriodDuration(newDebatingPeriodDuration);

    const [debatingPeriod, minimumQuorum] = await dao.getConfig();

    expect(newDebatingPeriodDuration).to.eq(debatingPeriod);
    expect(minimumQuorum).to.eq(defaultMinimumQuorum);
  });

  it("Success update minimum quorum", async function () {
    const newMinQuoroum = 10;
    await dao.connect(chairman).updateMinimumQuorum(newMinQuoroum);

    const [debatingPeriod, minimumQuorum] = await dao.getConfig();

    expect(debatingPeriod).to.eq(defaultDebatinPeriodDurationSeconds);
    expect(minimumQuorum).to.eq(newMinQuoroum);
  });

  it("Can't update minimum quorum to more then 100 percent", async function () {
    const newMinQuoroum = 101;
    await expect(dao.connect(chairman).updateMinimumQuorum(newMinQuoroum)).to.be.revertedWith("Quorum percent must be less then 100");

    const [debatingPeriod, minimumQuorum] = await dao.getConfig();

    expect(debatingPeriod).to.eq(defaultDebatinPeriodDurationSeconds);
    expect(minimumQuorum).to.eq(defaultMinimumQuorum);
  });

  it("Only chairman can update config", async function () {
    const [newDebatingPeriodDuration, newMinQuoroum] = [1000, 10];
    await expect(dao.connect(owner).updateMinDebatingPeriodDuration(newDebatingPeriodDuration)).to.be.revertedWith("Only chairman action");
    await expect(dao.connect(owner).updateMinimumQuorum(newMinQuoroum)).to.be.revertedWith("Only chairman action");

    const [debatingPeriod, minimumQuorum] = await dao.getConfig();

    expect(debatingPeriod).to.eq(defaultDebatinPeriodDurationSeconds);
    expect(minimumQuorum).to.eq(defaultMinimumQuorum);
  });

  it("Get quorum amount from percent", async function () {
    const requiredQuorumPercent = bigInt("10");
    const quorumAmount = await dao.connect(chairman).getQuorumAmount(requiredQuorumPercent);

    expect(quorumAmount).to.eq(initialTokenBalance.div(bigInt("10")))
  });

  it("Only chairman can add proposal", async function () {
    await expect(dao.addProposal(testAddress, testCallData, testDescription)).to.be.revertedWith("Only chairman action");
  });

  it("Can't add proposal with zero address", async function () {
    await expect(dao.connect(chairman).addProposal(zeroAddress, testCallData, testDescription)).to.be.revertedWith("Require non zero address");
  });

  it("Success add proposal", async function () {
    const tx = await dao.connect(chairman).addProposal(testAddress, testCallData, testDescription);
    const latestTimestamp = await latestBlockTimestamp();

    await expect(tx).to.emit(dao, "NewProposal").withArgs(
      1,
      [
        testAddress,
        testCallData,
        testDescription,
        latestTimestamp,
        defaultMinimumQuorum,
        defaultDebatinPeriodDurationSeconds,
        Status.Started,
        bigInt("0"),
        bigInt("0")
      ]
    );

    const currentProposalID = await dao.proposalIDCounter();
    const newProposal = await dao.proposals(currentProposalID);

    expect(newProposal.recepient).to.eq(testAddress);
    expect(newProposal.callData).to.eq(testCallData);
    expect(newProposal.description).to.eq(testDescription);
    expect(newProposal.startTime).to.eq(latestTimestamp);
    expect(newProposal.minimumQuorum).to.eq(defaultMinimumQuorum);
    expect(newProposal.debatingPeriodDuration).to.eq(defaultDebatinPeriodDurationSeconds);
    expect(newProposal.status).to.eq(Status.Started);
    expect(newProposal.forVotes.toString()).to.eq("0");
    expect(newProposal.againstVotes.toString()).to.eq("0");
  });

  it("Can't vote with zero deposit", async function () {
    await expect(dao.vote(1, true)).to.be.revertedWith("Zero deposit");
  });

  it("Can't vote for unexist proposal", async function () {
    await deposit(owner, bigInt("1000"));
    await expect(dao.vote(1, true)).to.be.revertedWith("Unexist proposal");
  });

  it("Can't vote for unexist proposal", async function () {
    await deposit(owner, bigInt("1000"));
    await expect(dao.vote(1, true)).to.be.revertedWith("Unexist proposal");
  });

  it("Can't vote if debating period is over", async function () {
    await deposit(owner, bigInt("1000"));
    await addProposal(testAddress, testCallData, testDescription);
    await increaseTime(defaultDebatinPeriodDurationSeconds);

    await expect(dao.vote(1, true)).to.be.revertedWith("Debating period is over");
  });

  it("Can't double vote for same proposal", async function () {
    await deposit(owner, bigInt("1000"));
    await addProposal(testAddress, testCallData, testDescription);

    await dao.vote(1, true);
    expect((await dao.voted(1, owner.address))).to.eq(true);

    await expect(dao.vote(1, true)).to.be.revertedWith("Already voted for this proposal");
  });

  it("Success vote for proposal", async function () {
    const fullDeposit = bigInt("1000");

    await deposit(owner, fullDeposit);
    await addProposal(testAddress, testCallData, testDescription);
    const propBefore = await dao.proposals(1);

    expect((await dao.voted(1, owner.address))).to.eq(false);
    const tx = await dao.vote(1, true);
    const propAfter = await dao.proposals(1);
    const newWithdrawAt = await dao.withdrawAt(owner.address);

    await expect(tx).to.emit(dao, "NewVote").
      withArgs(1, owner.address, true, fullDeposit);
    expect((await dao.voted(1, owner.address))).to.eq(true);
    expect(propAfter.forVotes).to.eq(propBefore.forVotes.add(fullDeposit));
    expect(propAfter.againstVotes).to.eq(propBefore.againstVotes);
    expect(newWithdrawAt).to.eq(propAfter.startTime.add(propAfter.debatingPeriodDuration))
  });

  it("Success vote against proposal", async function () {
    const fullDeposit = bigInt("1000");

    await deposit(owner, fullDeposit);
    await addProposal(testAddress, testCallData, testDescription);
    const propBefore = await dao.proposals(1);

    expect((await dao.voted(1, owner.address))).to.eq(false);
    const tx = await dao.vote(1, false);
    const propAfter = await dao.proposals(1);

    await expect(tx).to.emit(dao, "NewVote").
      withArgs(1, owner.address, false, fullDeposit);
    expect((await dao.voted(1, owner.address))).to.eq(true);
    expect(propAfter.againstVotes).to.eq(propBefore.againstVotes.add(fullDeposit));
    expect(propAfter.forVotes).to.eq(propBefore.forVotes);
  });

  it("Can't finish unexist proposal", async function () {
    await expect(dao.finish(1)).to.be.revertedWith("Unexist proposal");
  });

  it("Can't finish if debating period not over", async function () {
    await addProposal(testAddress, testCallData, testDescription);

    await expect(dao.finish(1)).to.be.revertedWith("Debating period is not over");
  });

  it("Can't finish finished proposal", async function () {
    await addProposal(testAddress, testCallData, testDescription);
    await increaseTime(defaultDebatinPeriodDurationSeconds);

    await dao.finish(1)

    await expect(dao.finish(1)).to.be.revertedWith("Proposal finished");
  });

  it("Finish with not enough quorum", async function () {
    await addProposal(testAddress, testCallData, testDescription);
    await vote(1, owner, true, bigInt("99"))
    await increaseTime(defaultDebatinPeriodDurationSeconds);
    const expectedStatus = Status.NotEnoughQuorum;

    const tx = await dao.finish(1);
    await expect(tx).to.emit(dao, "Finish").withArgs(1, expectedStatus);
    const proposal = await dao.proposals(1);

    expect(proposal.status).to.eq(expectedStatus);
  });

  it("Finish with not enough for votes", async function () {
    await addProposal(testAddress, testCallData, testDescription);

    const halfOfMinQuorumAmount = await dao.getQuorumAmount(defaultMinimumQuorum / 2);
    await vote(1, owner, true, halfOfMinQuorumAmount);
    await vote(1, addr1, false, halfOfMinQuorumAmount);
    await increaseTime(defaultDebatinPeriodDurationSeconds);
    const expectedStatus = Status.NotEnoughForVotes;

    const tx = await dao.finish(1);
    await expect(tx).to.emit(dao, "Finish").withArgs(1, expectedStatus);
    const proposal = await dao.proposals(1);

    expect(proposal.status).to.eq(expectedStatus);
  });

  it("Finish with external contract call execution success", async function () {
    const callData = genTestContractCallData("setVariable1", [999])
    const halfOfMinQuorumAmount = await dao.getQuorumAmount(defaultMinimumQuorum / 2);

    await addProposal(testAddress, callData, testDescription);
    await vote(1, owner, true, halfOfMinQuorumAmount.add(bigInt("1")))
    await vote(1, addr1, false, halfOfMinQuorumAmount)
    await increaseTime(defaultDebatinPeriodDurationSeconds);
    const expectedStatus = Status.Finished;

    const testValueBefore = await testContract.variable1();

    const tx = await dao.finish(1);
    await expect(tx).to.emit(dao, "Finish").withArgs(1, expectedStatus);
    const proposal = await dao.proposals(1);
    const testValueAfter = await testContract.variable1();

    expect(proposal.status).to.eq(expectedStatus);
    expect(testValueAfter).to.not.eq(testValueBefore);
    expect(testValueAfter).to.eq(bigInt("999"));
  });

  it("Finish with external contract call execution fail", async function () {
    const callData = genTestContractCallData("alwaysFailMethod", undefined)
    const halfOfMinQuorumAmount = await dao.getQuorumAmount(defaultMinimumQuorum / 2);

    await addProposal(testAddress, callData, testDescription);
    await vote(1, owner, true, halfOfMinQuorumAmount.add(bigInt("1")))
    await vote(1, addr1, false, halfOfMinQuorumAmount)
    await increaseTime(defaultDebatinPeriodDurationSeconds);
    const expectedStatus = Status.ExecutionError;

    const tx = await dao.finish(1);
    await expect(tx).to.emit(dao, "Finish").withArgs(1, expectedStatus);
    const proposal = await dao.proposals(1);

    expect(proposal.status).to.eq(expectedStatus);
  });
});
