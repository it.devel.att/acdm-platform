//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./Interfaces.sol";

contract StakingMock is StakingPublic {
    mapping(address => uint256) deposits;

    function updateDeposit(address _account, uint256 _amount) external {
        deposits[_account] = _amount;
    }

    function depositAmount(address _account)
        external
        view
        override(StakingPublic)
        returns (uint256)
    {
        return deposits[_account];
    }
}
