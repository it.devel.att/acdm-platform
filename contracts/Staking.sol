//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./Interfaces.sol";

contract Staking is StakingPublic {
    // XXXToken
    IERC20 public rewardToken;
    // LP Token (vote token also)
    IERC20 public stakingToken;

    DAOPublic public dao;

    mapping(address => Staker) public stakers;
    struct Staker {
        uint256 stakeBalance;
        uint256 rewardBalance;
        uint256 lastStakeTimestamp;
        uint256 lastRewardUpdateTimestamp;
    }

    // Каждую неделю пользователям начисляются награда, 3% от их вклада
    uint256 public _percent = 3;
    uint256 public _rewardSeconds = 1 weeks;
    uint256 public _freezeStakeSeconds = 1 days;

    mapping(address => bool)  private _admins;

    constructor(address _stakingToken, address _rewardToken) {
        stakingToken = IERC20(_stakingToken);
        rewardToken = IERC20(_rewardToken);
        _admins[msg.sender] = true;
    }

    modifier onlyAdmin {
        require(_admins[msg.sender], "Only admin action");
        _;
    }

    modifier onlyDAO {
        require(address(dao) == msg.sender, "Only DAO action");
        _;
    }

    function updateDao(address _dao) public onlyAdmin {
        dao = DAOPublic(_dao);
    }

    function updatePercent(uint256 newPercent) public onlyDAO {
        _percent = newPercent;
    }

    function updateRewardSeconds(uint256 _newRewardSeconds) public onlyAdmin {
        _rewardSeconds = _newRewardSeconds;
    }

    function updateFreezeStakeSeconds(uint256 _newFreezeStakeSeconds) public onlyAdmin {
        _freezeStakeSeconds = _newFreezeStakeSeconds;
    }

    function stake(uint256 _amount) external {
        Staker memory staker = stakers[msg.sender];

        staker.rewardBalance += calculateReward(staker);
        staker.lastRewardUpdateTimestamp = block.timestamp;
        staker.stakeBalance += _amount;
        staker.lastStakeTimestamp = block.timestamp;

        stakers[msg.sender] = staker;
        stakingToken.transferFrom(msg.sender, address(this), _amount);
    }
    
    function unstake() external {
        Staker memory staker = stakers[msg.sender];

        require(staker.lastStakeTimestamp != 0, "Never stake");
        require(staker.lastStakeTimestamp <= block.timestamp - _freezeStakeSeconds, "Freeze time don't pass");
        require(staker.stakeBalance != 0, "Zero balance");
        require(!dao.depositInActiveVote(msg.sender), "Cannot unstake while active vote");

        uint256 balance = staker.stakeBalance;
        staker.stakeBalance = 0;
        stakers[msg.sender] = staker;
        stakingToken.transfer(msg.sender, balance);
    }

    function claim() external {
        Staker memory staker = stakers[msg.sender];
        uint256 rewardBalance = staker.rewardBalance + calculateReward(staker);
        staker.rewardBalance = 0;
        staker.lastRewardUpdateTimestamp = block.timestamp;
        stakers[msg.sender] = staker;

        rewardToken.transfer(msg.sender, rewardBalance); 
    }

    function calculateReward(Staker memory _staker) internal view returns(uint256) {
        uint256 lastTimestamp = _staker.lastRewardUpdateTimestamp;
        if (lastTimestamp == 0) {
            return 0;
        }
        uint256 secondsPassFromLastStake = block.timestamp - lastTimestamp;
        uint256 times = secondsPassFromLastStake / _rewardSeconds;
        return times * calculatePercent(_staker.stakeBalance);
    }

    function calculatePercent(uint256 _amount) internal view returns(uint256) {
        uint256 amount = _amount * 1000;
        uint256 percentAmount = amount / 100 * _percent;
        return percentAmount / 1000;
    }

    // StakingPublic inteface implementations

    function depositAmount(address _account) external view override(StakingPublic) returns(uint256) {
        return stakers[_account].stakeBalance;
    }
}
