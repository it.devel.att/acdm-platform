//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

contract Test {
    address private _dao;

    uint256 public variable1;
    address public variable2;

    constructor() {}

    modifier onlyDao() {
        require(msg.sender == _dao, "Only dao action");
        _;
    }

    function setDao(address dao) external {
        _dao = dao;
    }

    function setVariable1(uint256 newValue) external onlyDao {
        variable1 = newValue;
    }

    function setVariable1AndVariable2(uint256 newValue, address newAddress)
        external
        onlyDao
    {
        variable1 = newValue;
        variable2 = newAddress;
    }

    function alwaysFailMethod() external onlyDao {
        require(true == false, "Very important error");
        variable1 = 1;
    }
}
