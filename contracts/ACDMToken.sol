//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ACDMToken is ERC20 {
    address public acdmPlatform;

    constructor(address _platform) ERC20("ACADEM Coin", "ACDM") {
        acdmPlatform = _platform;
    }

    function decimals() public pure override(ERC20) returns (uint8) {
        return 6;
    }

    modifier onlyPlatform() {
        require(msg.sender == acdmPlatform, "Only ACDM platfrom action");
        _;
    }

    function mint(uint256 _amount) external onlyPlatform {
        _mint(msg.sender, _amount);
    }

    function burn(uint256 _amount) external onlyPlatform {
        _burn(msg.sender, _amount);
    }
}
