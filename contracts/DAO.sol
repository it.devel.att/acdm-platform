//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./Interfaces.sol";

contract DAO is DAOPublic {
    IERC20 private voteToken;

    StakingPublic private stakingContract;

    // TODO Use access control
    address private chairman;

    uint256 private _config;

    uint256 public proposalIDCounter;

    mapping(address => uint256) public deposits;

    mapping(address => uint256) public withdrawAt;

    mapping(uint256 => Proposal) public proposals;
    struct Proposal {
        address recepient;
        bytes callData;
        string description;
        uint256 startTime;
        uint256 minimumQuorum;
        uint256 debatingPeriodDuration;
        Status status;
        uint256 forVotes;
        uint256 againstVotes;
    }
    enum Status {
        Started,
        Finished,
        NotEnoughQuorum,
        NotEnoughForVotes,
        ExecutionError
    }
    mapping(uint256 => mapping(address => bool)) public voted;

    event NewProposal(uint256 id, Proposal proposal);
    event NewVote(
        uint256 proposalID,
        address voting,
        bool voteFor,
        uint256 amount
    );
    event Finish(uint256 proposalID, Status status);

    constructor(
        address _chairman,
        address _voteToken, // This is LP token
        uint256 _minimumQuorum,
        uint256 _debatingPeriodDuration
    ) {
        chairman = _chairman;
        voteToken = IERC20(_voteToken);
        setConfig(_debatingPeriodDuration, _minimumQuorum);
    }

    modifier onlyChairman() {
        require(msg.sender == chairman, "Only chairman action");
        _;
    }

    function updateMinimumQuorum(uint256 newQuorum) external onlyChairman {
        (uint256 currentDebatinPeriod, ) = getConfig();
        setConfig(currentDebatinPeriod, newQuorum);
    }

    function updateMinDebatingPeriodDuration(uint256 newPeriod)
        external
        onlyChairman
    {
        (, uint256 currentQuorumPercent) = getConfig();
        setConfig(newPeriod, currentQuorumPercent);
    }

    function setStakingContract(address _stakingContract) external onlyChairman {
        stakingContract = StakingPublic(_stakingContract);
    }

    function getConfig()
        public
        view
        returns (uint256 debatingPeriod, uint256 quorumPercent)
    {
        uint256 config_ = _config;
        debatingPeriod = config_ >> 128;
        quorumPercent = config_ & uint256(type(uint128).max);
    }

    function setConfig(uint256 debatingPeriod, uint256 quorumPercent) internal {
        require(quorumPercent <= 100, "Quorum percent must be less then 100");
        _config = (debatingPeriod << 128) + quorumPercent;
    }

    function getQuorumAmount(uint256 quorumPecent)
        public
        view
        returns (uint256)
    {
        return (voteToken.totalSupply() / 100) * quorumPecent;
    }

    function addProposal(
        address _recepient,
        bytes calldata _callData,
        string memory _description
    ) external onlyChairman {
        require(address(0) != _recepient, "Require non zero address");
        (uint256 debatingPeriodDuration, uint256 minimumQuorum) = getConfig();

        proposalIDCounter++;
        Proposal memory proposal = Proposal({
            recepient: _recepient,
            callData: _callData,
            description: _description,
            startTime: block.timestamp,
            minimumQuorum: minimumQuorum,
            debatingPeriodDuration: debatingPeriodDuration,
            status: Status.Started,
            forVotes: 0,
            againstVotes: 0
        });
        proposals[proposalIDCounter] = proposal;

        emit NewProposal(proposalIDCounter, proposal);
    }

    function vote(uint256 _proposalID, bool forVote) external {
        uint256 fullDeposit = stakingContract.depositAmount(msg.sender);
        require(fullDeposit != 0, "Zero deposit!");

        Proposal memory proposal = proposals[_proposalID];

        require(proposal.startTime != 0, "Unexist proposal");
        require(
            proposal.startTime + proposal.debatingPeriodDuration >
                block.timestamp,
            "Debating period is over"
        );
        require(
            !voted[_proposalID][msg.sender],
            "Already voted for this proposal"
        );
        if (forVote) {
            proposal.forVotes += fullDeposit;
        } else {
            proposal.againstVotes += fullDeposit;
        }
        voted[_proposalID][msg.sender] = true;
        uint256 nextWithdrawAt = proposal.startTime +
            proposal.debatingPeriodDuration;

        if (withdrawAt[msg.sender] < nextWithdrawAt) {
            withdrawAt[msg.sender] = nextWithdrawAt;
        }
        proposals[_proposalID] = proposal;

        emit NewVote(_proposalID, msg.sender, forVote, fullDeposit);
    }

    function finish(uint256 _proposalID) external {
        Proposal memory proposal = proposals[_proposalID];

        require(proposal.startTime != 0, "Unexist proposal");
        require(
            proposal.startTime + proposal.debatingPeriodDuration <=
                block.timestamp,
            "Debating period is not over"
        );
        require(proposal.status == Status.Started, "Proposal finished");

        uint256 allVotes = proposal.forVotes + proposal.againstVotes;
        if (allVotes < getQuorumAmount(proposal.minimumQuorum)) {
            proposal.status = Status.NotEnoughQuorum;
            _saveProposalAndEmitEvent(_proposalID, proposal);
            return;
        }
        if (proposal.forVotes <= proposal.againstVotes) {
            proposal.status = Status.NotEnoughForVotes;
            _saveProposalAndEmitEvent(_proposalID, proposal);
            return;
        }
        proposal.status = _callSignature(proposal.recepient, proposal.callData);
        _saveProposalAndEmitEvent(_proposalID, proposal);
    }

    function _saveProposalAndEmitEvent(uint256 _id, Proposal memory _proposal)
        internal
    {
        proposals[_id] = _proposal;
        emit Finish(_id, _proposal.status);
    }

    function _callSignature(address _recepient, bytes memory _signature)
        internal
        returns (Status)
    {
        (bool success, ) = _recepient.call{value: 0}(_signature);
        return success ? Status.Finished : Status.ExecutionError;
    }

    function depositInActiveVote(address _account) external view override(DAOPublic) returns(bool) {
        return block.timestamp < withdrawAt[_account];
    }
}
