//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./Interfaces.sol";

contract DAOMock is DAOPublic {
    mapping(address => bool) activeVotes;

    function setDepositInActiveVote(address _account, bool _active) external {
        activeVotes[_account] = _active;
    }

    function depositInActiveVote(address _account)
        external
        view
        override(DAOPublic)
        returns (bool)
    {
        return activeVotes[_account];
    }

    function callThroughDAO(address _contract, bytes calldata _signature) external returns(bool) {
        (bool success, ) = _contract.call{value: 0}(_signature);
        return success;
    }
}
