//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

interface DAOPublic {
    function depositInActiveVote(address _account) external view returns (bool);
}

interface StakingPublic {
    function depositAmount(address _account) external view returns (uint256);
}
