//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./Interfaces.sol";
import "./ACDMToken.sol";

error PercentExceedAmount(uint16 maxAmount);
error ZeroAddress();
error ZeroAmount();
error AddressSameAsSender();
error PlatformAlreadyStarted();
error NotSaleRound();
error NotTradeRound();
error PlatformNotStartYet();
error RoundNotEnded();
error RoundEnded();
error NotOrderOwner();
error NotEnoughTokensLeft();
error NotEnoughWei();
error SendedWeiNotEqRequired(uint256 requiredWei);
error OnlyDAOAction();
error OnlyOwnerAction();

contract ACDMPlatform {
    enum RoundType {
        Sale,
        Trade
    }
    event NewOrder(uint256 id, TradeOrder order);
    event OrderWithdraw(uint256 id, TradeOrder order);
    event OrderBuy(
        uint256 id,
        TradeOrder order,
        uint256 amount,
        uint256 msgValue
    );

    event NewSaleRound(uint256 id, SaleStat saleStat);

    address public owner;
    address public dao;
    ACDMToken public token;

    uint256 public saleRoundCounter;
    uint256 public tradeRoundCounter;

    RoundType public currentRound = RoundType.Sale;

    uint256 public roundDuration = 3 days;

    // round => statistic
    mapping(uint256 => SaleStat) public saleStats;
    // round => statistic
    mapping(uint256 => TradeStat) public tradeStats;

    struct SaleStat {
        uint256 startTime;
        uint256 initialTokenAmount;
        uint256 leftTokens;
        uint256 weiForToken;
    }

    struct TradeStat {
        uint256 startTime;
        uint256 weiTradeAmount;
    }

    uint256 public ordersCounter;
    // orderId => order
    mapping(uint256 => TradeOrder) public tradeOrders;
    struct TradeOrder {
        address owner;
        uint256 tokenAmount;
        uint256 priceForToken;
        bool active;
    }

    mapping(address => address) public referrers;

    uint16 public tradeRefersPercent = 25; // 2.5%
    uint16 public buyTokensReferrer1Percent = 50; // 5%
    uint16 public buyTokensReferrer2Percent = 30; // 3%

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        if (msg.sender != owner) revert OnlyOwnerAction();
        _;
    }

    modifier onlyDAO() {
        if (msg.sender != dao) revert OnlyDAOAction();
        _;
    }

    function updateDaoAddress(address _dao) external onlyOwner {
        dao = _dao;
    }

    uint16 maxPercent = 1000;

    function updateTradeRefersPercent(uint16 _newPercent) external onlyDAO {
        if (_newPercent > maxPercent) revert PercentExceedAmount(maxPercent);
        tradeRefersPercent = _newPercent;
    }

    function updateBuyTokensReferrer1Percent(uint16 _newPercent)
        external
        onlyDAO
    {
        if (_newPercent > maxPercent) revert PercentExceedAmount(maxPercent);
        buyTokensReferrer1Percent = _newPercent;
    }

    function updateBuyTokensReferrer2Percent(uint16 _newPercent)
        external
        onlyDAO
    {
        if (_newPercent > maxPercent) revert PercentExceedAmount(maxPercent);
        buyTokensReferrer2Percent = _newPercent;
    }

    function transferBalanceToOwner() external onlyDAO {
        payable(owner).transfer(address(this).balance);
    }

    //TODO Buy XXX Token on uniswap and burn it
    // function uniswapBuyTokensAndBurn() external onlyDAO {
    // }

    function register(address _referrer) external {
        if (_referrer == address(0)) revert ZeroAddress();
        if (_referrer == msg.sender) revert AddressSameAsSender();
        referrers[msg.sender] = _referrer;
    }

    function startPlatform(address _token) external onlyOwner {
        if (saleRoundCounter != 0) revert PlatformAlreadyStarted();

        token = ACDMToken(_token);
        uint256 tokenAmount = 100_000;
        uint256 _priceForToken = 1 ether / tokenAmount;

        saleRoundCounter++;
        token.mint(tokenAmount);

        saleStats[saleRoundCounter] = SaleStat({
            startTime: block.timestamp,
            initialTokenAmount: tokenAmount,
            leftTokens: tokenAmount,
            weiForToken: _priceForToken
        });
    }

    function buyTokens(uint256 _amount) external payable {
        if (saleRoundCounter == 0) revert PlatformNotStartYet();
        if (currentRound != RoundType.Sale) revert NotSaleRound();

        SaleStat memory saleStat = _currentSaleStat();
        if (saleStat.startTime + roundDuration <= block.timestamp)
            revert RoundEnded();
        if (saleStat.leftTokens < _amount) revert NotEnoughTokensLeft();

        uint256 requiredETH = saleStat.weiForToken * _amount;
        if (msg.value != requiredETH)
            revert SendedWeiNotEqRequired(requiredETH);

        address referrer1 = referrers[msg.sender];
        if (referrer1 != address(0)) {
            uint256 referrer1PercentAmount = calculatePercent(
                msg.value * 10,
                buyTokensReferrer1Percent
            ) / 100;
            payable(referrer1).transfer(referrer1PercentAmount);

            address referrer2 = referrers[referrer1];
            if (referrer2 != address(0)) {
                uint256 referrer2PercentAmount = calculatePercent(
                    msg.value * 10,
                    buyTokensReferrer2Percent
                ) / 100;
                payable(referrer2).transfer(referrer2PercentAmount);
            }
        }

        token.transfer(msg.sender, _amount);
        saleStat.leftTokens -= _amount;

        _saveCurrentSaleStat(saleStat);
    }

    function finishSaleRoundAndStartTradeRound() external {
        if (saleRoundCounter == 0) revert PlatformNotStartYet();
        if (currentRound != RoundType.Sale) revert NotSaleRound();

        SaleStat memory saleStat = _currentSaleStat();
        if (saleStat.startTime + roundDuration > block.timestamp)
            revert RoundNotEnded();
        token.burn(saleStat.leftTokens);

        currentRound = RoundType.Trade;
        tradeRoundCounter++;

        tradeStats[tradeRoundCounter] = TradeStat({
            startTime: block.timestamp,
            weiTradeAmount: 0
        });
    }

    function finishTradeRoundAndStartSaleRound() external {
        if (currentRound != RoundType.Trade) revert NotTradeRound();

        TradeStat memory tradeStat = _currentTradeStat();
        if (tradeStat.startTime + roundDuration > block.timestamp)
            revert RoundNotEnded();
        currentRound = RoundType.Sale;
        SaleStat memory saleStat = _currentSaleStat();

        uint256 _priceForToken = priceForToken(saleStat.weiForToken);
        uint256 tokenAmount = tradeStat.weiTradeAmount / _priceForToken;

        saleRoundCounter++;
        token.mint(tokenAmount);

        saleStats[saleRoundCounter] = SaleStat({
            startTime: block.timestamp,
            initialTokenAmount: tokenAmount,
            leftTokens: tokenAmount,
            weiForToken: _priceForToken
        });
        emit NewSaleRound(saleRoundCounter, saleStats[saleRoundCounter]);
    }

    function createOrder(uint256 _amount, uint256 _weiPriceForToken) external {
        if (currentRound != RoundType.Trade) revert NotTradeRound();
        if (_amount == 0) revert ZeroAmount();

        token.transferFrom(msg.sender, address(this), _amount);
        ordersCounter++;

        tradeOrders[ordersCounter] = TradeOrder({
            owner: msg.sender,
            tokenAmount: _amount,
            priceForToken: _weiPriceForToken,
            active: true
        });
        emit NewOrder(ordersCounter, tradeOrders[ordersCounter]);
    }

    function withdrawOrder(uint256 _id) external {
        TradeOrder memory tradeOrder = tradeOrders[_id];
        if (tradeOrder.owner != msg.sender) revert NotOrderOwner();
        if (tradeOrder.tokenAmount == 0) revert ZeroAmount();

        token.transfer(msg.sender, tradeOrder.tokenAmount);
        tradeOrder.tokenAmount = 0;
        tradeOrder.active = false;

        tradeOrders[_id] = tradeOrder;
        emit OrderWithdraw(_id, tradeOrder);
    }

    function buyOrder(uint256 _id, uint256 _amount) external payable {
        if (currentRound != RoundType.Trade) revert NotTradeRound();
        TradeOrder memory tradeOrder = tradeOrders[_id];
        if (tradeOrder.tokenAmount < _amount) revert NotEnoughTokensLeft();

        uint256 totalPrice = tradeOrder.priceForToken * _amount;
        if (msg.value != totalPrice) revert NotEnoughWei();

        tradeOrder.tokenAmount -= _amount;
        if (tradeOrder.tokenAmount == 0) {
            tradeOrder.active = false;
        }

        address payable orderOwner = payable(tradeOrder.owner);

        token.transfer(msg.sender, _amount);
        uint256 transferToOwner = msg.value;
        uint256 refersPercent = calculatePercent(
            msg.value * 10,
            tradeRefersPercent
        ) / 100;
        transferToOwner -= (refersPercent * 2);

        address referrer1 = referrers[tradeOrder.owner];
        if (referrer1 != address(0)) {
            payable(referrer1).transfer(refersPercent);

            address referrer2 = referrers[referrer1];
            if (referrer2 != address(0)) {
                payable(referrer2).transfer(refersPercent);
            }
        }
        orderOwner.transfer(transferToOwner);

        tradeOrders[_id] = tradeOrder;

        TradeStat memory tradeStat = _currentTradeStat();
        tradeStat.weiTradeAmount += msg.value;
        _saveCurrentTradeStat(tradeStat);

        emit OrderBuy(_id, tradeOrder, _amount, msg.value);
    }

    function calculatePercent(uint256 _amount, uint256 _percent)
        internal
        pure
        returns (uint256)
    {
        uint256 amount = _amount * 1000;
        uint256 percentAmount = (amount / 100) * _percent;
        return percentAmount / 1000;
    }

    function _currentSaleStat() internal view returns (SaleStat memory) {
        return saleStats[saleRoundCounter];
    }

    function _saveCurrentSaleStat(SaleStat memory _stat) internal {
        saleStats[saleRoundCounter] = _stat;
    }

    function _currentTradeStat() internal view returns (TradeStat memory) {
        return tradeStats[tradeRoundCounter];
    }

    function _saveCurrentTradeStat(TradeStat memory _stat) internal {
        tradeStats[saleRoundCounter] = _stat;
    }

    uint256 internal formulaVar1 = 103; // 1,03
    uint256 internal formulaVar2 = 4_000_000_000_000; // 0,000004

    // lastPrice*1,03+0,000004
    function priceForToken(uint256 _lastPrice) public view returns (uint256) {
        return ((_lastPrice * formulaVar1) / 100) + formulaVar2;
    }
}
